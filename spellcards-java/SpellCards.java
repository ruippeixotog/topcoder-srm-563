public class SpellCards {

	public int maxDamage(int[] level, int[] damage) {
		int best = maxDamageSeq(level, damage);
		for (int i = 1; i < level.length; i++) {
			shift(level, damage);
			best = Math.max(best, maxDamageSeq(level, damage));
		}
		return best;
	}

	public void shift(int[] level, int[] damage) {
		if (level.length == 0)
			return;
		int temp = level[0];
		int temp2 = damage[0];
		for (int i = 1; i < level.length; i++) {
			level[i - 1] = level[i];
			damage[i - 1] = damage[i];
		}
		level[level.length - 1] = temp;
		damage[level.length - 1] = temp2;
	}

	public int maxDamageSeq(int[] level, int[] damage) {
		int[][] maxDamage = new int[level.length + 1][level.length];

		for (int i = level.length - 1; i >= 0; i--) {
			for (int j = 0; j < level.length - i; j++) {
				calcMaxDamage(level, damage, maxDamage, i, j);
			}
		}
		return maxDamage[0][0];
	}

	private void calcMaxDamage(int[] level, int[] damage, int[][] maxDamage,
			int k, int cl) {

		int best = maxDamage[k + 1][Math.max(cl - 1, 0)];
		if (k + level[k] - 1 < level.length
				&& cl + level[k] - 1 < level.length - k) {
			best = Math.max(best, damage[k]
					+ maxDamage[k + 1][cl + level[k] - 1]);
		}
		maxDamage[k][cl] = best;
	}
}
