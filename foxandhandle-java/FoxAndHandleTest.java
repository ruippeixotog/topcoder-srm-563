import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FoxAndHandleTest {

    protected FoxAndHandle solution;

    @Before
    public void setUp() {
        solution = new FoxAndHandle();
    }

    @Test
    public void testCase0() {
        String S = "foxfox";

        String expected = "fox";
        String actual = solution.lexSmallestName(S);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        String S = "ccieliel";

        String expected = "ceil";
        String actual = solution.lexSmallestName(S);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        String S = "abaabbab";

        String expected = "aabb";
        String actual = solution.lexSmallestName(S);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        String S = "bbbbaaaa";

        String expected = "bbaa";
        String actual = solution.lexSmallestName(S);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        String S = "fedcbafedcba";

        String expected = "afedcb";
        String actual = solution.lexSmallestName(S);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase5() {
        String S = "nodevillivedon";

        String expected = "deilvon";
        String actual = solution.lexSmallestName(S);

        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testCase10() {
        String S = "adadaddebecbaedeebbadadaddebecbaedeebb";

        String expected = "aaaabbddddebecbedee";
        String actual = solution.lexSmallestName(S);

        Assert.assertEquals(expected, actual);
    }
}
