public class FoxAndHandle {
	
	public String lexSmallestName(String S) {
		int[] count = new int[26];
		for (int i = 0; i < S.length(); i++) {
			count[S.charAt(i) - 'a']++;
		}
		for (int i = 0; i < 26; i++) {
			count[i] /= 2;
		}
		return lexSmallestName(S, count, count.clone(), "", 0);
	}

	String lexSmallestName(String S, int[] sCount, int[] rCount, String currH,
			int k) {
		for (int i = 0; i < sCount.length; i++) {
			if (sCount[i] == 0) {
				continue;
			}
			int next = nextValidPos(S, sCount, rCount, i, k);
			if (next != -1) {
				sCount[i]--;
				return lexSmallestName(S, sCount, rCount, currH
						+ (char) (i + 'a'), next + 1);
			}
		}
		return currH;
	}

	int nextValidPos(String S, int[] sCount, int[] rCount, int letter, int k) {
		if (k == S.length()) {
			return -1;
		}
		if (S.charAt(k) - 'a' == letter) {
			return isValid(S, sCount, rCount, k) ? k : -1;
		}
		if (rCount[S.charAt(k) - 'a'] == 0) {
			return -1;
		}
		rCount[S.charAt(k) - 'a']--;
		int value = nextValidPos(S, sCount, rCount, letter, k + 1);
		if (value == -1) {
			rCount[S.charAt(k) - 'a']++;
		}
		return value;
	}

	boolean isValid(String S, int[] sCount, int[] rCount, int k) {
		if (k == S.length()) {
			return true;
		}
		int[] arr = rCount;
		if(arr[S.charAt(k) - 'a'] == 0) {
			arr = sCount;
			if(arr[S.charAt(k) - 'a'] == 0) {
				return false;
			}
		}
		arr[S.charAt(k) - 'a']--;
		boolean value = isValid(S, sCount, rCount, k + 1);
		arr[S.charAt(k) - 'a']++;
		return value;
	}
}
