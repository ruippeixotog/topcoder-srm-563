public class SpellCardsEasy {

	public int maxDamage(int[] level, int[] damage) {
		int[][] maxDamage = new int[level.length + 1][level.length];

		for (int i = level.length - 1; i >= 0; i--) {
			for (int j = 0; j < level.length - i; j++) {
				calcMaxDamage(level, damage, maxDamage, i, j);
			}
		}
		return maxDamage[0][0];
	}

	private void calcMaxDamage(int[] level, int[] damage, int[][] maxDamage,
			int k, int cl) {

		int best = maxDamage[k + 1][Math.max(cl - 1, 0)];
		if (k + level[k] - 1 < level.length
				&& cl + level[k] - 1 < level.length - k) {
			best = Math.max(best, damage[k]
					+ maxDamage[k + 1][cl + level[k] - 1]);
		}
		maxDamage[k][cl] = best;
	}
}
