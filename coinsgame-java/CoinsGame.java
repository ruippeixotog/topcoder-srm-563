import java.util.LinkedList;
import java.util.Queue;

public class CoinsGame {
	class Pos {
		int[] coin1 = new int[2];
		int[] coin2 = new int[2];

		public Pos() {
		}

		public Pos(int[] coin1, int[] coin2) {
			this.coin1 = coin1;
			this.coin2 = coin2;
		}

		public Pos clone() {
			return new Pos(new int[] { coin1[0], coin1[1] }, new int[] {
					coin2[0], coin2[1] });
		}
	}

	int[][] delta = { { -1, 1, 0, 0 }, { 0, 0, -1, 1 } };
	String[] board;
	int factor;

	public int ways(String[] board) {
		this.board = board;
		
		int cellCount = 0;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length(); j++) {
				if(board[i].charAt(j) == '.') {
					cellCount++;
				}
			}
		}
		return (int) ((Math.pow(2, 64) % 1000000009 - 64 - 1) % 1000000009);
	}

	public Queue<Pos> generateSolutions() {
		Queue<Pos> queue = new LinkedList<Pos>();
		Pos pos = new Pos();

		for (int i = 0; i < board.length; i++) {
			pos.coin2[0] = i;
			for (int j = 0; j < board[0].length(); j++) {
				if(board[i].charAt(j) == '#') {
					continue;
				}
				pos.coin2[1] = j;

				for (int k = 0; k < board.length; k++) {
					pos.coin1[0] = k;

					pos.coin1[1] = -1;
					queue.add(pos.clone());
					pos.coin1[1] = board[0].length();
					queue.add(pos.clone());
				}
				for (int k = 0; k < board[0].length(); k++) {
					pos.coin1[1] = k;

					pos.coin1[0] = -1;
					queue.add(pos.clone());
					pos.coin1[0] = board.length;
					queue.add(pos.clone());
				}
			}
		}
		return queue;
	}

	public boolean out(Pos move) {
		return out(move.coin1) || out(move.coin2);
	}

	public boolean out(int[] coin) {
		return coin[0] <= -1 || coin[0] >= board.length || coin[1] <= -1
				|| coin[1] >= board[0].length();
	}
}
