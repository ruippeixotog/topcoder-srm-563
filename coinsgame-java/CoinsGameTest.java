import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CoinsGameTest {

    protected CoinsGame solution;

    @Before
    public void setUp() {
        solution = new CoinsGame();
    }

    @Test
    public void testCase0() {
        String[] board = new String[]{".."};

        int expected = 1;
        int actual = solution.ways(board);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        String[] board = new String[]{"##.#", ".###", "###.", "#.##"};

        int expected = 11;
        int actual = solution.ways(board);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        String[] board = new String[]{"####", "#..#", "#..#", "####"};

        int expected = 0;
        int actual = solution.ways(board);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        String[] board = new String[]{"#.#.#"};

        int expected = 0;
        int actual = solution.ways(board);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        String[] board = new String[]{"........", "........", "........", "........", "........", "........", "........", "........"};

        int expected = 688856388;
        int actual = solution.ways(board);

        Assert.assertEquals(expected, actual);
    }

}
