public class FoxAndHandleEasy {

	public String isPossible(String S, String T) {
		int idx = T.indexOf(S, 0);
		while (idx != -1) {
			if ((T.substring(0, idx) + T.substring(idx + S.length())).equals(S))
				return "Yes";
			idx = T.indexOf(S, idx + 1);
		}
		return "No";
	}
}
