public class CoinsGameEasy {

	class Pos {
		int row1, col1, row2, col2;

		public Pos() {
		}

		public Pos(int row1, int col1, int row2, int col2) {
			this.row1 = row1;
			this.col1 = col1;
			this.row2 = row2;
			this.col2 = col2;
		}
	}

	public int minimalSteps(String[] board) {
		Pos pos = find(board);
		for (int i = 1; i <= 10; i++) {
			if (dfs(board, pos, i)) {
				return i;
			}
		}
		return -1;
	}

	public boolean dfs(String[] board, Pos pos, int steps) {
		if (steps == 0) {
			return solution(pos);
		}
		return dfs(board, left(board, pos), steps - 1)
				|| dfs(board, right(board, pos), steps - 1)
				|| dfs(board, up(board, pos), steps - 1)
				|| dfs(board, down(board, pos), steps - 1);
	}

	public Pos find(String[] board) {
		Pos pos = new Pos();
		boolean first = true;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length(); j++) {
				if (board[i].charAt(j) == 'o') {
					if (first) {
						pos.row1 = i;
						pos.col1 = j;
						board[i] = board[i].replaceFirst("o", ".");
						first = false;
					} else {
						pos.row2 = i;
						pos.col2 = j;
						board[i] = board[i].replaceFirst("o", ".");
						return pos;
					}
				}
			}
		}
		return pos;
	}

	public boolean solution(Pos move) {
		return move.col1 == -1 && move.col2 != -1 || move.col1 != -1
				&& move.col2 == -1 || move.row1 == -1 && move.row2 != -1
				|| move.row1 != -1 && move.row2 == -1;
	}

	public boolean out(Pos move) {
		return move.col1 == -1 || move.col2 == -1 || move.row1 == -1
				|| move.row2 == -1;
	}

	public Pos left(String[] board, Pos pos) {
		if(out(pos)) return pos;
		
		Pos newPos = new Pos();
		newPos.row1 = pos.row1;
		newPos.row2 = pos.row2;
		if (pos.col1 <= 0) {
			newPos.col1 = -1;
		} else {
			if (board[pos.row1].charAt(pos.col1 - 1) != '#') {
				newPos.col1 = pos.col1 - 1;
			} else {
				newPos.col1 = pos.col1;
			}
		}
		if (pos.col2 <= 0) {
			newPos.col2 = -1;
		} else {
			if (board[pos.row2].charAt(pos.col2 - 1) != '#') {
				newPos.col2 = pos.col2 - 1;
			} else {
				newPos.col2 = pos.col2;
			}
		}
		return newPos;
	}

	public Pos right(String[] board, Pos pos) {
		if(out(pos)) return pos;
		
		Pos newPos = new Pos();
		newPos.row1 = pos.row1;
		newPos.row2 = pos.row2;
		if (pos.row1 == -1 || pos.col1 >= board[0].length() - 1) {
			newPos.col1 = -1;
		} else {
			if (board[pos.row1].charAt(pos.col1 + 1) != '#') {
				newPos.col1 = pos.col1 + 1;
			} else {
				newPos.col1 = pos.col1;
			}
		}
		if (pos.row2 == -1 || pos.col2 >= board[0].length() - 1) {
			newPos.col2 = -1;
		} else {
			if (board[pos.row2].charAt(pos.col2 + 1) != '#') {
				newPos.col2 = pos.col2 + 1;
			} else {
				newPos.col2 = pos.col2;
			}
		}
		return newPos;
	}

	public Pos up(String[] board, Pos pos) {
		Pos newPos = new Pos();
		newPos.col1 = pos.col1;
		newPos.col2 = pos.col2;
		if (pos.col1 == -1 || pos.row1 <= 0) {
			newPos.row1 = -1;
		} else {
			if (board[pos.row1 - 1].charAt(pos.col1) != '#') {
				newPos.row1 = pos.row1 - 1;
			} else {
				newPos.row1 = pos.row1;
			}
		}
		if (pos.col2 == -1 || pos.row2 <= 0) {
			newPos.row2 = -1;
		} else {
			if (board[pos.row2 - 1].charAt(pos.col2) != '#') {
				newPos.row2 = pos.row2 - 1;
			} else {
				newPos.row2 = pos.row2;
			}
		}
		return newPos;
	}

	public Pos down(String[] board, Pos pos) {
		Pos newPos = new Pos();
		newPos.col1 = pos.col1;
		newPos.col2 = pos.col2;
		if (pos.col1 == -1 || pos.row1 >= board.length - 1) {
			newPos.row1 = -1;
		} else {
			if (board[pos.row1 + 1].charAt(pos.col1) != '#') {
				newPos.row1 = pos.row1 + 1;
			} else {
				newPos.row1 = pos.row1;
			}
		}
		if (pos.col2 == -1 || pos.row2 >= board.length - 1) {
			newPos.row2 = -1;
		} else {
			if (board[pos.row2 + 1].charAt(pos.col2) != '#') {
				newPos.row2 = pos.row2 + 1;
			} else {
				newPos.row2 = pos.row2;
			}
		}
		return newPos;
	}
}
